import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Simple from './pages/simplePage';
import NotFound from './pages/notFound';
import Layout from './components/Layout';

class App extends React.Component {

    render() {

        return (
            <BrowserRouter>
                <Route component={Layout}>
                    <Switch>
                        <Layout path='/' exact component={Simple} />
                        <Layout component={NotFound} />
                    </Switch>
                </Route>
            </BrowserRouter>
        );

    }
}

export default App;
