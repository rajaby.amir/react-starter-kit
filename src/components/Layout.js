import React from 'react';
import { Route } from 'react-router-dom';

const Layout = ({ children, ...rest }) => {
    return (
        <div>
            {children}
        </div>
    );
};

export default ({ component: Component, ...rest }) => {
    return (
        <Route {...rest} render={matchProps => (
            <Layout>
                <Component {...matchProps} />
            </Layout>
        )} />
    )
};