import React from 'react';
import { simpleAction } from '../actions/simpleAction';
import { connect } from 'react-redux'

class Simple extends React.Component {
    simpleAction = (event) => {
        this.props.simpleAction();
    }

    render() {
        return (
            <div className="App">
                <button onClick={this.simpleAction}>Test redux action</button>

                <pre>
                    {
                        JSON.stringify(this.props)
                    }
                </pre>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    ...state
});
const mapDispatchToProps = dispatch => ({
    simpleAction: () => dispatch(simpleAction())
})

export default connect(mapStateToProps, mapDispatchToProps)(Simple);
